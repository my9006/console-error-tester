package base;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.service.DriverService;

public class ProjectWebDriver extends ChromeDriver {
    private static ProjectWebDriver instance;
    private ChromeOptions options;
    private ChromeDriverService chromeDriverService;

    private ProjectWebDriver(ChromeDriverService chromeDriverService, ChromeOptions options) {
        super(chromeDriverService, options);
        this.options=options;
        this.chromeDriverService = chromeDriverService;
    }


    private static void setProperty() {
        System.setProperty("webdriver.chrome.driver", OsDriverPropertyProvider.getPath());
        System.setProperty("webdriver.chrome.silentOutput", "true");
    }

    public static ProjectWebDriver getWebDriver() {
        if (instance == null) {
            synchronized (ProjectWebDriver.class) {
                if (instance == null) {
                    setProperty();
                    DriverService.Builder serviceBuilder = new ChromeDriverService.Builder().withSilent(true);
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("--log-level=3");
                    options.addArguments("--silent");
                    ChromeDriverService chromeDriverService = (ChromeDriverService)serviceBuilder.build();
                    instance = new ProjectWebDriver(chromeDriverService, options);
                }
            }
        }
        return instance;
    }

}
