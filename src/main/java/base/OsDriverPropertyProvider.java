package base;

public class OsDriverPropertyProvider {
    private static String driverPath;

    public final static String getPath() {
        String winDriverPath = "src/main/drivers/chromedriver_win.exe";
        String macDriverPath = "src/main/drivers/chromedriver_mac";
        String linuxDriverPath = "src/main/drivers/chromedriver_linux";
        String currentOs = System.getProperty("os.name").toLowerCase();
        boolean isOsRecognized = false;
        if (currentOs.contains("win")) {
            driverPath = winDriverPath;
        } else if (currentOs.contains("mac")) {
            driverPath = macDriverPath;
        } else if (currentOs.contains("nix") || currentOs.contains("nux") || currentOs.contains("aix")) {
            driverPath = linuxDriverPath;
        } else {
            throw new NullPointerException("Driver for current OS cannot be found");
        }
        return driverPath;
    }
}
