package base;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ProjectWait {

    private static long timeOut= 30;
    private static final ProjectWebDriver driver=ProjectWebDriver.getWebDriver();
    private static ProjectWait instance=null;
    private final WebDriverWait wait;

    private ProjectWait() {
        wait = new WebDriverWait(driver, Duration.ofSeconds(timeOut));
    }

    public static ProjectWait getInstance(){
        if(instance==null){
            synchronized (ProjectWait.class){
                if(instance==null){
                    instance= new ProjectWait();
                }
            }
        }
        return instance;
    }

    public void waitForPage(WebDriver driver){
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
    }

    public WebElement waitForElement(WebElement element){
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    public WebElement waitForElement(By by){
        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }
}
