package base;

import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.v86.log.Log;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.logging.Level;

public class LogCapturer {

    DevTools devTools;

    public void captureLogs(ProjectWebDriver driver) {

        LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);

        for (LogEntry entry: logEntries){
            if(entry.getLevel().equals(Level.SEVERE)) {
                System.out.println("\u001B[36m ********************************************************");
                System.out.println("\u001B[36m ********************************************************");
                System.out.println("\u001B[0m ********************************************************");
                throw  new AssertionError(entry.getMessage());
            }
        }
    }

}
