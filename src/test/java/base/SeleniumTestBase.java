package base;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;

public class SeleniumTestBase {
    private ProjectWebDriver webDriver;

    public ProjectWebDriver getWebDriver() {
        return webDriver;
    }

    @AfterMethod(alwaysRun = true)
    public void teardown() {
        webDriver.quit();
    }

}
