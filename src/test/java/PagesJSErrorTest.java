import base.LogCapturer;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import static base.ProjectWebDriver.*;
public class PagesJSErrorTest {

//    @Test(dataProvider = "pages_for_js_error", dataProviderClass = PagesDataProvider.class)
    @Test
    public void checkForJSErrors(String url) {
        getWebDriver().get(url);
        LogCapturer logger = new LogCapturer();
        logger.captureLogs(getWebDriver());
    }

    @AfterTest(alwaysRun = true)
    public void tearDown(){

        getWebDriver().close();
        getWebDriver().quit();
    }

}
